================================================
Downloads and binary installers
================================================

.. contents:: Binary Downloads Table of Contents

^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Ubuntu packages
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. image:: http://i.imgur.com/m6ihRGz.png

The Quasarware Framework PPA contains deb packages for the Quasar framework and various Quasar toolkits. Quasar_Studio SDR and a slew of hardware support drivers are provided by the MyriadRF SDR Drivers PPA.

* https://github.com/Quasarware/QuasarCore/wiki/Ubuntu

^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
openSUSE packages
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. image:: http://i.imgur.com/rre4Avj.png

This openSUSE repository contains packages for a variety of software in the SDR ecosystem including Quasar and Quasar_StudioSDR. Special thanks to Martin Hauke for maintaining this repository:

* https://build.opensuse.org/project/show/hardware:sdr

^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Windows installer
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. image:: http://i.imgur.com/Ce24OeT.png

The Quasar SDR development environment contains pre-built windows binaries of Quasar and Quasar_StudioSDR along with a number of hardware drivers and the GNU Radio toolkit.

Follow the tutorial steps:

* https://github.com/Quasarware/QuasarSDR/wiki

Or go direct to downloads:

* http://downloads.myriadrf.org/builds/QuasarSDR/?C=M;O=D

^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
OSX homebrew
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. image:: http://i.imgur.com/gLxShgE.jpg

Install Quasar, Quasar_StudioSDR, and various toolkits, using the homebrew package manager.

* https://github.com/Quasarware/homebrew-Quasar/wiki

^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
FreeBSD ports
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. image:: http://jabirproject.org/wp-content/uploads/2013/05/Freebsd-logo.png

Please note that the sghctoma are a work in progress.

* Qwt6 for Qt5: https://www.mail-archive.com/kde-freebsd@kde.org/msg21200.html
* Quasar, Quasar_StudioSDR, spuce, muparserx: https://github.com/sghctoma/freebsd-ports
