.. contents:: Build guide table of contents

-------------------------------------------------------------------------
Get the dependencies
-------------------------------------------------------------------------

The main Quasar project really only requires CMake and a compiler.
Additional components require additional dependencies.
If those dependencies are not available, the component does not build.

* CMake
* C++11 compiler
* python development files for python bindings

  * and numpy as a runtime requirement
* qt5 development files for Quasar-gui
* portaudio for audio support
* graphviz for topology debug view
* `Quasar_StudioSDR`_ for SDR device support

.. _`Quasar_StudioSDR`: https://github.com/Quasarware/Quasar_StudioSDR/wiki/BuildGuide

^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Ubuntu
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. image:: http://i.imgur.com/rzuCBUr.png

Recommended apt-get install instructions for Ubuntu:
::

    sudo apt-get install \
        libnuma-dev cmake g++ \
        libpython-dev python-numpy \
        qtbase5-dev libqt5svg5-dev libqt5opengl5-dev libqwt-qt5-dev \
        portaudio19-dev libjack-jackd2-dev \
        graphviz

Save some time: Even if you are building Quasar from source,
you may still want to install some of the build dependencies from our PPA:
::

    sudo add-apt-repository -y ppa:Quasarware/support
    sudo add-apt-repository -y ppa:Quasarware/framework
    sudo apt-get update
    sudo apt-get install \
        libpoco-dev \
        libspuce-dev \
        nlohmann-json-dev \ #nlohmann-json3-dev on 20.04 lts
        libmuparserx-dev

    #pre-packaged Quasar_Studio SDR development files
    sudo add-apt-repository -y ppa:myriadrf/drivers
    sudo apt-get update
    sudo apt-get install libsoapysdr-dev

^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Fedora
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. image:: http://icons-for-free.com/free-icons/png/64/395212.png

Some packages are optional based on required features:
::

    sudo dnf -y install cmake g++ numactl-devel \
        python3 python3-numpy \
        qt5-qtbase-devel qt5-qtsvg-devel qwt-qt5-devel \
        portaudio-devel-19 jack-audio-connection-kit-devel graphviz

^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Windows
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. image:: http://i.imgur.com/YAu30uL.png

* CMake - http://www.cmake.org/cmake/resources/software.html
* Visual studio 2013 or later

*Optional dependencies based on your needs:*

* Python (2.7 and up) - https://www.python.org/downloads/windows/
* Numpy (must match python version) - http://www.lfd.uci.edu/~gohlke/pythonlibs/#numpy
* JDK - http://www.oracle.com/technetwork/java/javase/downloads/index.html
* QT5 (must match MSVC version) - http://qt-project.org/downloads

^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
OSX
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. image:: http://i.imgur.com/gLxShgE.jpg

**Step 1)** Install Xcode Command Line Tools: https://railsapps.github.io/xcode-command-line-tools.html

**Step 2)** Install Quasar, various toolkits, and dependencies
with a few simple commands using the `Quasar Homebrew tap`_.

.. _`Quasar Homebrew tap`: https://github.com/Quasarware/homebrew-Quasar/wiki

----

-------------------------------------------------------------------------
Get the source code
-------------------------------------------------------------------------

.. image:: http://i.imgur.com/YImPftq.png

**First time repository clone from git**
::

    #git clone with recursive submodule checkout
    git clone --recursive https://github.com/Quasarware/QuasarCore.git

**Update from an existing checkout**
::

    cd QuasarCore

    #update to latest master branch
    git pull origin master

    #update submodules to latest tracking branch
    git submodule update --init --recursive --remote

----

------------------------------------------------------------------------
Build and install
------------------------------------------------------------------------

.. image:: http://i.imgur.com/n7WReFx.png

^^^^^^^^^^^^^^^^^^^^^^^^^
Unix instructions
^^^^^^^^^^^^^^^^^^^^^^^^^

::

    mkdir build
    cd build
    cmake ..
    make -j4
    sudo make install
    sudo ldconfig #needed on debian systems
    QuasarUtil --self-tests
    QuasarFlow #launches GUI designer

^^^^^^^^^^^^^^^^^^^^^^^^^
Windows instructions
^^^^^^^^^^^^^^^^^^^^^^^^^

.. image:: https://developer.valvesoftware.com/w/images/6/6a/Visual_studio_logo.png

After configuring with CMake open a Visual studio development terminal and run:
::

    cmake --build my_build_dir --config Release
    cmake --build my_build_dir --config Release --target install

* add C:\\Program Files\\Quasar to the %PATH%
* open cmd and run QuasarUtil.exe --self-tests
* open cmd and run QuasarFlow.exe

^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Environment variables
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The Quasar library needs to know its installation path.
The library is compiled with the CMAKE_INSTALL_PREFIX
as the default search path for modules and library resources.
However, if the library is installed to a different location other than the one it was configured with,
the POTHOS_ROOT environment variable can be set to reflect the new installation path.

^^^^^^^^^^^^^^^^^^^^^^^^^^
Build with debug symbols
^^^^^^^^^^^^^^^^^^^^^^^^^^

::

    cmake .. -DCMAKE_BUILD_TYPE=Debug


^^^^^^^^^^^^^^^^^^^^^^^^^^
Custom install prefix
^^^^^^^^^^^^^^^^^^^^^^^^^^

::

    cmake .. -DCMAKE_INSTALL_PREFIX=/opt/Quasar
