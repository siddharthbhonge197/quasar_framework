=======================================
Roadmap major features
=======================================

.. contents:: Roadmap table of contents

^^^^^^^^^^^^^^^^^^^^^^^^^^
Performance metrics
^^^^^^^^^^^^^^^^^^^^^^^^^^

--------------------
Metrics API
--------------------
Expose additonal performance metrics through API.
Currently we collect basic thoughput per port.
More detail is needed, and short-run, long-run averages.

--------------------
GUI Support
--------------------
Expose live performance metrics into the graphical tool.
Metrics could be displayed on the connection as a color or as a throughput number.
Or add additional dock panels to enumerate and display metrics.

^^^^^^^^^^^^^^^^^^^^^^^^^^
Hardware demos
^^^^^^^^^^^^^^^^^^^^^^^^^^

We are looking to support and demonstrate Quasar on a variety of intersting hardware platforms.
The following is a list of planned demonstrations.
Futher suggestions and hardware donations are welcome.

--------------------
Quasar FPGA project
--------------------

* Use the `Quasar FPGA project`_ to control a topology of processing blocks within the FPGA.
  The connections in the topology will automatically configure the routing within the FPGA.
  Use the topology and virtual channels to "snoop" on flows within the FPGA.

* Zero-copy scheduler buffer integration to shared memory/DMA
  flows to/from the FPGA using the custom buffer API.
  This also implies automatic GPP ingress and outgress when FPGA blocks
  from the `Quasar FPGA project`_ are connected to GPP blocks.

.. _`Quasar FPGA project`: https://github.com/Quasarware/QuasarFPGA/wiki

--------------------
Zynq demo
--------------------

* Demonstrate using the GUI and framework to remotely deploy a topology onto an ARM board.
  Using the graphical widgets to control and monitor the remote topology.

* Demo buffer integration and the `Quasar FPGA project`_ on Zynq through shared memory

--------------------
PCIe demo
--------------------

* Demo buffer integration and the `Quasar FPGA project`_ through PCIe DMA.

^^^^^^^^^^^^^^^^^^^^^^^^^^
Language bindings
^^^^^^^^^^^^^^^^^^^^^^^^^^

----------
Java
----------
Finish the Java-facing bindings so that Java can call into the Quasar Proxy API.
And create java-style wrapper for Quasar::Block so the Block API
best matches accepted coding practices in Java.

----------
C#
----------
C# bindings, both through mono and windows managed code.

^^^^^^^^^^^^^^^^^^^^^^^^^^
GUI Improvements
^^^^^^^^^^^^^^^^^^^^^^^^^^

--------------------
Hierarchy design
--------------------
Using the graphical tool to create topological hierarchies of processing blocks.
Hierarchies can be included in top level designs, and deployed on available hosts.
Modifications to hierarchies should cause re-evaluation of client topologies.

--------------------
Block designer
--------------------
Create a processing block in one of the supported languages.
The block will be compiled and deployed on available hosts.

^^^^^^^^^^^^^^^^^^^^^^^^^^
Wishlist
^^^^^^^^^^^^^^^^^^^^^^^^^^

--------------------
Topology redundancy
--------------------
If a node in the network goes down, automatically redeploy the design on the next available node.

--------------------
Flow constraints
--------------------
Allow the user to describe an expected thoughput, or latency.
Attempt to alter parameters to meet the constraint;
such as modifying the buffer sizer per work()
or applying high priority, or migrating hosts.