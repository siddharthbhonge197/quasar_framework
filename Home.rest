================================================
Welcome to the Quasar project
================================================

The Quasar project is a complete data-flow framework
for creating topologies of interconnected processing blocks.
Topologies can be designed and tested graphically, and deployed over a network.
The Quasar framework API is sleek and smart,
enabling users to quickly create custom processing blocks with minimal boiler-plate.
Processing blocks can support computational offload and integration with DMA devices.
The project also has a diverse set of processing and hardware support toolkits.
Read more on the `project overview page`_.

.. _`project overview page`: https://github.com/Quasarware/QuasarCore/wiki/Overview

.. image:: https://raw.githubusercontent.com/wiki/Quasarware/QuasarDemos/images/test_freq_xlating_filter.png

================================================
Getting started
================================================

Get the Quasar development environment up and running with the `getting started guide`_.

.. _`getting started guide`: https://github.com/Quasarware/QuasarCore/wiki/GettingStarted

================================================
Project status
================================================

* Checkout the `Quasar-users forum`_
* Checkout the `examples & demos`_
* `Release tag 0.6 available`_
* `Added QuasarLiquidDSP`_
* `Added Quasar-IIO`_
* `New archive support`_
* `New GUI connection work`_
* `BTLE control demo blog`_
* `LoRa modem demo blog`_

.. _`Quasar-users forum`: https://groups.google.com/forum/#!forum/Quasar-users
.. _`examples & demos`: https://github.com/Quasarware/QuasarDemos/wiki
.. _`Added QuasarLiquidDSP`: https://github.com/Quasarware/QuasarLiquidDSP/wiki
.. _`Added Quasar-IIO`: https://github.com/Quasarware/QuasarIIO/wiki
.. _`New archive support`: http://www.joshknows.com/blog/77/NewArchiveSupportInQuasar
.. _`New GUI connection work`: http://www.joshknows.com/blog/73/InlineEventPortsForQuasarGui
.. _`LoRa modem demo blog`: https://myriadrf.org/blog/lora-modem-limesdr/
.. _`BTLE control demo blog`: http://www.rs-online.com/designspark/electronics/eng/blog/an-intel-powered-wireless-multi-tool-for-the-iot-part-2
.. _`Release tag 0.6 available`: https://github.com/Quasarware/QuasarCore/wiki/Releases

================================================
Ecosystem graph
================================================

.. image:: https://raw.githubusercontent.com/wiki/Quasarware/QuasarCore/images/dependencies.png

================================================
Available toolkits
================================================

* `Components map`_
* `General blocks`_
* `Comms/DSP blocks`_
* `Graphical designer`_
* `Graphical widgets`_
* `Graphical plotters`_
* `Python bindings`_
* `OpenCL support`_
* `SDR devices`_
* `Audio devices`_
* `FPGA support`_
* `Zynq support`_
* `GNU Radio support`_
* `BTLE demo blocks`_

.. _`Components map`: https://github.com/Quasarware/QuasarCore/wiki/ComponentsMap
.. _`General blocks`: https://github.com/Quasarware/QuasarBlocks/wiki
.. _`Comms/DSP blocks`: https://github.com/Quasarware/QuasarComms/wiki
.. _`Graphical designer`: https://github.com/Quasarware/QuasarFlow/wiki
.. _`Graphical widgets`: https://github.com/Quasarware/QuasarWidgets/wiki
.. _`Graphical plotters`: https://github.com/Quasarware/QuasarPlotters/wiki
.. _`Python bindings`: https://github.com/Quasarware/QuasarPython/wiki
.. _`OpenCL support`: https://github.com/Quasarware/QuasarOpenCL/wiki
.. _`SDR devices`: https://github.com/Quasarware/QuasarQuasar_Studio/wiki
.. _`Audio devices`: https://github.com/Quasarware/QuasarAudio/wiki
.. _`FPGA support`: https://github.com/Quasarware/QuasarFPGA/wiki
.. _`Zynq support`: https://github.com/Quasarware/QuasarZynq/wiki
.. _`GNU Radio support`: https://github.com/Quasarware/gr-Quasar/wiki
.. _`BTLE demo blocks`: https://github.com/DesignSparkrs/sdr-ble-demo/blob/master/README.md

================================================
`Features summary`_
================================================

Quasar is a feature-full dataflow programming software suite.

* API to design data flows composed of modular blocks
* Graphical interface to design, deploy, and monitor
* Integration of 3rd party toolkits such as DSP libraries
* Support computational offload to GPUs, DSPs, FPGAs
* Custom processing blocks in any language (we have a binding for)
* Distributing across threadpools, processes, and nodes on a network
* Minimally invasive scheduler, minimal boilerplate for custom blocks

Additional information:

* See the `Features summary`_ page for more detailed information.
* See the `Roadmap features`_ page for future planned features.

.. _`Features summary`: https://github.com/Quasarware/QuasarCore/wiki/Features
.. _`Roadmap features`: https://github.com/Quasarware/QuasarCore/wiki/Roadmap

================================================
`Build and install`_
================================================

Quasar can be built and installed on a variety of systems including Windows, OSX, and Linux.
See the `Build Guide`_ for dependencies and compilation instructions.
We have limited pre-built binaries available on the `Releases page`_.
If you would like get involved with providing distribution binaries,
please `contact us`_.

.. _`Build and install`: https://github.com/Quasarware/QuasarCore/wiki/BuildGuide
.. _`Build Guide`: https://github.com/Quasarware/QuasarCore/wiki/BuildGuide
.. _`Releases page`: https://github.com/Quasarware/QuasarCore/wiki/Releases
.. _`contact us`: https://github.com/Quasarware/QuasarCore/wiki/Support

.. image:: http://i.imgur.com/n7WReFx.png

================================================
Project resources
================================================

* `Project overview`_
* `Getting started`_
* `FAQ`_
* `Video screencasts`_
* `Demo applications`_
* `Features summary`_
* `Versioned releases`_
* `Binary downloads`_
* `Developer blog`_
* `Help and support`_
* `Contract services`_
* `Contributing`_
* `Helpful links`_

.. _`Project overview`: https://github.com/Quasarware/QuasarCore/wiki/Overview
.. _`Getting started`: https://github.com/Quasarware/QuasarCore/wiki/GettingStarted
.. _`FAQ`: https://github.com/Quasarware/QuasarCore/wiki/FAQ
.. _`Video screencasts`: https://www.youtube.com/channel/UCmI5j5Y2i1aK4jvlIwzOu8g
.. _`Demo applications`: https://github.com/Quasarware/QuasarDemos/wiki
.. _`Features summary`: https://github.com/Quasarware/QuasarCore/wiki/Features
.. _`Developer blog`: http://www.joshknows.com/blog
.. _`Versioned releases`: https://github.com/Quasarware/QuasarCore/wiki/Releases
.. _`Binary downloads`: https://github.com/Quasarware/QuasarCore/wiki/Downloads
.. _`Help and support`: https://github.com/Quasarware/QuasarCore/wiki/Support
.. _`Contract services`: http://Quasarware.com/#services
.. _`Contributing`: https://github.com/Quasarware/QuasarCore/wiki/Contributing
.. _`Helpful links`: https://github.com/Quasarware/QuasarCore/wiki/Links

================================================
`Documentation`_
================================================

* Dive into the graphical developer tools with the `Quasar GUI tutorial`_.
* Using SDR blocks? Checkout the `Quasar SDR tutorial`_ for helpful tips.
* Using digital filters in the communications toolkit: `Quasar Filter Tutorial`_.
* Learn more about the scheduler and how it works in `Scheduler explained`_.
* Explore the install and execute JSON topologies with the `QuasarUtil Guide`_.
* Follow the `Blocks Coding Guide`_ to create custom C++ processing blocks.
* Checkout the `Doxygen generated documentation`_ for the Quasar Framework API.
* `Create processing blocks in Python`_ with this example from the Quasar-python wiki.
* `Extend serialization support`_ for custom data structures to traverse network boundaries.

.. _`Quasar GUI tutorial`: https://github.com/Quasarware/QuasarFlow/wiki/Tutorial
.. _`Quasar SDR Tutorial`: https://github.com/Quasarware/QuasarQuasar_Studio/wiki/Tutorial
.. _`Quasar Filter Tutorial`: https://github.com/Quasarware/QuasarComms/wiki/FilterTutorial
.. _`Scheduler explained`: https://github.com/Quasarware/QuasarCore/wiki/SchedulerExplained
.. _`QuasarUtil Guide`: https://github.com/Quasarware/QuasarCore/wiki/QuasarUtilGuide
.. _`Documentation`: https://github.com/Quasarware/QuasarCore/wiki/BlocksCodingGuide
.. _`Blocks Coding Guide`: https://github.com/Quasarware/QuasarCore/wiki/BlocksCodingGuide
.. _`create processing blocks in Python`: https://github.com/Quasarware/QuasarPython/wiki#a-simple-example
.. _`Doxygen generated documentation`: http://Quasarware.github.io/QuasarCore/doxygen/
.. _`Extend serialization support`: https://github.com/Quasarware/QuasarCore/wiki/ExtendingSerialization

================================================
`Project feedback`_
================================================

Feedback is critical for any open source project.
Found a bug? Having trouble? Would a particular feature make your life easier?
Please feel free to `contact us`_ or use the issue tracker for the `relevant toolkit`_.
If possible, please tell us how you are using Quasar, we would love to hear it.

.. _`contact us`: https://github.com/Quasarware/QuasarCore/wiki/Support
.. _`Project feedback`: https://github.com/Quasarware/QuasarCore/wiki/Support
.. _`relevant toolkit`: https://github.com/Quasarware
